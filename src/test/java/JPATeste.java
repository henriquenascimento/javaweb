import javax.persistence.EntityManager;

import br.com.www.automovel.domain.Automovel;
import br.com.www.automovel.util.Factory;

public class JPATeste {
	
	public static void main(String[] args){
		
		EntityManager em = Factory.getEntityManager();
		em.getTransaction().begin();
		
		//Busca o autom�vel
		Automovel autoDetached = em.find(Automovel.class, 3);
		
		//Desanexa o autom�vel
		em.detach(autoDetached);
		
		//Modificamos o objeto que est� em detached
		autoDetached.setMarca("Porsche 911 Turbo");
		
		//Buscamos uma inst�ncia gerenciada da mesma informa��o
		Automovel autoGerenciado = em.find(Automovel.class, 3);
		
		//Mofificamos o que acabamos de buscar
		autoGerenciado.setMarca("Porsche 911 Turbo");
		
		//Reanexa 
		
		
		
		
		
	}

}
