package br.com.www.automovel.bean;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import br.com.www.automovel.domain.Automovel;
import br.com.www.automovel.util.Factory;

@ManagedBean
public class AutomovelBean implements Serializable {
	private Automovel automovel = new Automovel();
	private List<Automovel> automoveis;

	public Automovel getAutomovel() {
		return automovel;
	}
	
	public void setAutomovel(Automovel automovel) {
		this.automovel = automovel;
	}
	
	public List<Automovel> getAutomoveis() {
		EntityManager em = Factory.getEntityManager();
		Query q = em.createQuery("select a from Automovel a",Automovel.class);
		this.automoveis = q.getResultList();
		return automoveis;
	}
		
	public void save(Automovel automovel){	
		EntityManager em = Factory.getEntityManager();
		em.getTransaction().begin();
		em.persist(automovel);
		em.getTransaction().commit();
		em.close();		
	}
	
	public void delete(Automovel automovel){
		EntityManager em = Factory.getEntityManager();
		EntityTransaction tx = em.getTransaction();
	
		tx.begin();
		automovel = em.merge(automovel);
		em.remove(automovel);
		tx.commit();
		em.close();
	}
	
	public Automovel getById(int id){
		EntityManager em = Factory.getEntityManager();
		return em.find(Automovel.class, id);
	}
	
	public void update(Automovel automovel, String value){
		EntityManager em = Factory.getEntityManager();
		Automovel autoAtualizado = em.find(Automovel.class, automovel.getId());
		autoAtualizado.setMarca( value );
		em.getTransaction().begin();
		em.merge(autoAtualizado);
		em.getTransaction().commit();
		em.close();
		
		/*
		EntityManager em = Factory.getEntityManager();
		Automovel autoDetached = em.find(Automovel.class, automovel.getId());
		em.getTransaction().begin();
		em.detach(autoDetached);
		Automovel atualizado = em.merge(autoDetached);
		System.out.println(atualizado.getId());
		System.exit(0);
		em.getTransaction().commit();
		em.close();
		*/
	}	
}