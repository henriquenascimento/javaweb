package br.com.www.automovel.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Automovel {
	
	@Id
	@GeneratedValue	
	private Integer id;
	private String marca;
	
	public Automovel(){}
		
	public Integer getId(){
		return id;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}		
}
